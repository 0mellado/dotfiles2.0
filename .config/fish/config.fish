if status is-interactive
    # Commands to run in interactive sessions can go here

# Manual aliases
	alias vi '/usr/bin/nvim'
	alias ll 'lsd -lh --group-dirs=first'
	alias la 'lsd -a --group-dirs=first'
	alias l 'lsd --group-dirs=first'
	alias lla 'lsd -lha --group-dirs=first'
	alias ls 'lsd --group-dirs=first'
	alias cat 'bat'
	alias tree 'exa -T'
	thefuck --alias | source

# functions
	function rmk
		scrub -p dod $argv
		shred -zun 10 -v $argv
	end
	
	function pillan
		export TERM='linux'
		ssh omellado@190.102.240.121
	end
end
