function fish_prompt --description 'Write out the prompt'
    set -l last_status $status

    prompt_login

    # PWD
    set_color '#0346FE' 
    echo -n ' '(prompt_pwd)
    set_color '#3f9c12'

    __terlar_git_prompt
    fish_hg_prompt
    echo

    if not test $last_status -eq 0
        set_color $fish_color_error
    end

    echo -n '➤ '
    set_color normal
end
