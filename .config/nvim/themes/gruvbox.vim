if (empty($TMUX))
    if (has("nvim"))
        let $NVIM_TUI_ENABLE_TRUE_COLOR=1
    endif

    if (has("termguicolors"))
        set termguicolors
    endif
endif

syntax on

let g:gruvbox_bold=1
let g:gruvbox_transparent_bg=0
let g:gruvbox_italic=1

colorscheme gruvbox 
