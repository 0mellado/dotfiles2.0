call plug#begin('~/.config/nvim/plugged')
    " Comment code
    Plug 'tpope/vim-commentary'
    " Syntax support
    Plug 'sheerun/vim-polyglot'
    Plug 'EddyBer16/pseint.vim'
    " Autopairs
    Plug 'jiangmiao/auto-pairs'
    " File explorer
    Plug 'scrooloose/NERDTree'    
    " Icons
    Plug 'ryanoasis/vim-devicons'
    " Intellisense
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' }
    Plug 'vim-scripts/restore_view.vim'
    Plug 'vim-scripts/auto-pairs-gentle'
    " Airline
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    " Git integration
    Plug 'mhinz/vim-signify'
    " Autoclose tags
    Plug 'alvan/vim-closetag'
    " Ranger
    Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}
    Plug 'nvim-lua/plenary.nvim'
    Plug 'nvim-telescope/telescope.nvim'
    " Fzf
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'airblade/vim-rooter'
    " Prettier
    Plug 'prettier/vim-prettier', { 'do': 'yarn install' }

    " Themes
    Plug 'joshdick/onedark.vim'
    Plug 'morhetz/gruvbox'
call plug#end()
