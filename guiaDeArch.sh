# Guia de instalación de Arch Linux en asus g14

rmmod nouveau

# Conexión Inalámbrica
loadkeys es
iwctl
device list
station NOMBRE_DISPOSITIVO scan  # El nombre sale del comando anterior
station NOMBRE_DISPOSITIVO get-networks
station NOMBRE_DISPOSITIVO connect NOMBRE_ROUTER
exit
ping archlinux.org
timedatectl set-ntp true

# Crear y formatear particiones (comandos usados en mi caso)

cfdisk
# Se crean 4 particiones
# Una para el /boot, 512M
# Una para el /, 50G
# Una para el /home, lo que quede de espacio
# La última para el swap, 1G

mkfs.fat -F32 /dev/nvme0n1p1
mkfs.ext4 /dev/nvme0n1p2
mkfs.ext4 /dev/nvme0n1p3
mkswap /dev/nvme0n1p4
swapon /dev/nvme0n1p4
mount /dev/nvme0n1p2 /mnt
mkdir /mnt/home
mount /dev/nvme0n1p3 /mnt/home
mkdir /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot

# Para prevenir un error
pacman -Sy archlinux-keyring
# [Optional]
pacman -Sy acpi_call

# Añadir el server de arch asus a las mirrors
# En /etc/pacman.conf

[g14]
SigLevel = Optional TrustAll
Server = https://arch.asus-linux.org

[core]
Include = /etc/pacman.d/mirrorlist

[extra]
Include = /etc/pacman.d/mirrorlist

# Y en /etc/pacman.d/mirrorlist

Server = https://arch.asus-linux.org

# Instalar el kernel y el firmware

pacstrap /mnt base linux-g14 linux-g14-headers linux-firmware amd-ucode 
genfstab -U /mnt >> /mnt/etc/fstab 

# Configurar sistema

arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime
hwclock --systohc
pacman -S neovim 
nvim /etc/locale.gen  # Buscar en_US.UTF-8 UTF-8 y es_ES.UTF-8 UTF-8
locale-gen
echo "LANG=es_CL.UTF-8" > /etc/locale.conf
echo "KEYMAP=es" > /etc/vconsole.conf
echo "asus" > /etc/hostname
nvim /etc/hosts
passwd
pacman -S grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot
grub-mkconfig -o /boot/grub/grub.cfg
pacman -S networkmanager
systemctl enable NetworkManager
useradd -m usuario
passwd usuario
usermod -aG wheel,audio,video,storage usuario
pacman -S sudo
nvim /etc/sudoers
exit
umount -R /mnt
shutdown now

# Sacar USB y arrancar PC

# Instalar Entorno de Escritorio gnome

nmcli device wifi list
nmcli device wifi connect NOMBRE password CONTRASEÑA
ping archlinux.org
sudo pacman -S xorg gnome
